<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>My Account</title>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  </head>
  <body>
    <div class="container">
      <h1>My Account</h1>
      @include('navigation')
      Hello, {{ $user->email }}.
      <p>You last logged in at: {{ $user->updated_at }}</p>
    </div>
  </body>
</html>
