<?php

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login', 'LoginController@index');
Route::post('/login', 'LoginController@login');

Route::get('/signup', 'SignupController@index');
Route::post('/signup', 'SignupController@signup');

Route::get('/account', function() {
    return view('account', [
        'user' => Auth::user()
    ]);
})->middleware('protected');

Route::get('/logout', 'LoginController@logout');

// remove when done with testing
Route::get('/user', function() {
    return Auth::user();
});
